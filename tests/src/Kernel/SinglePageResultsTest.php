<?php

namespace Drupal\Tests\views_inject\Kernel;

use Drupal\views\Views;

/**
 * Tests views inject functionality on views with a single page.
 *
 * @group views_inject
 */
class SinglePageResultsTest extends InjectResultsTestBase {

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['test_inject'];

  /**
   * Test with the module's functionality disabled.
   */
  public function testDisplayExtender() {
    // Test view with disabled display extender.
    $view = Views::getView('test_inject');
    $view->initHandlers();
    $view->setDisplay('default');
    $this->assertShorthandResultset(
      $view,
      ['page', 'page']
    );

    // Test with default settings.
    $view = $this->getInjectEnabledTestView();
    $this->assertShorthandResultset(
      $view,
      ['article', 'page', 'article', 'page']
    );
  }

  /**
   * Tests different offset values and their effects on a single page.
   */
  public function testOffset() {
    // Test with positive offset.
    $view = $this->getInjectEnabledTestView(1);
    $this->assertShorthandResultset(
      $view,
      ['page', 'article', 'page', 'article']
    );

    // Test extra items showing up at the end of the view.
    $view = $this->getInjectEnabledTestView(2);
    $this->assertShorthandResultset(
      $view,
      ['page', 'page', 'article', 'article']
    );

    // Test offset greater than row count.
    $view = $this->getInjectEnabledTestView(5);
    $this->assertShorthandResultset(
      $view,
      ['page', 'page', 'article', 'article']
    );

    // Test offset greater than page size.
    $view = $this->getInjectEnabledTestView(100);
    $this->assertShorthandResultset(
      $view,
      ['page', 'page', 'article', 'article']
    );
  }

  /**
   * Tests different chunk size values and their effects on a single page.
   */
  public function testChunkSize() {
    // Test with increased chunk size.
    $view = $this->getInjectEnabledTestView(0, 2);
    $this->assertShorthandResultset(
      $view,
      ['article', 'article', 'page', 'page']
    );

    // Test with chunk size greater than result set.
    $view = $this->getInjectEnabledTestView(0, 5);
    $this->assertShorthandResultset(
      $view,
      ['article', 'article', 'page', 'page']
    );

    // Test with chunk size greater than page size.
    $view = $this->getInjectEnabledTestView(0, 100);
    $this->assertShorthandResultset(
      $view,
      ['article', 'article', 'page', 'page']
    );
  }

  /**
   * Tests different chunk distance values and their effects on a single page.
   */
  public function testChunkDistance() {
    // Test with increased chunk size.
    $view = $this->getInjectEnabledTestView(0, 1, 2);
    $this->assertShorthandResultset(
      $view,
      ['article', 'page', 'page', 'article']
    );

    // Test with chunk size greater than result set.
    $view = $this->getInjectEnabledTestView(0, 1, 5);
    $this->assertShorthandResultset(
      $view,
      ['article', 'page', 'page', 'article']
    );

    // Test with chunk size greater than page size.
    $view = $this->getInjectEnabledTestView(0, 1, 100);
    $this->assertShorthandResultset(
      $view,
      ['article', 'page', 'page', 'article']
    );
  }

}
