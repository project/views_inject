<?php

namespace Drupal\Tests\views_inject\Kernel;

use Drupal\views\ViewExecutable;

/**
 * Tests views inject functionality related to the pager.
 *
 * @group views_inject
 */
class PagerCountTest extends InjectResultsTestBase {

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['test_inject'];

  /**
   * {@inheritdoc}
   */
  protected int $nodeCount = 9;

  /**
   * Test result count in different paging scenarios.
   */
  public function testResultCount() {
    // Test result count on single page view.
    $view = $this->getPagedView();
    $this->executeView($view);
    $this->assertEquals(18, $view->getPager()->getTotalItems());
    $this->assertEquals(18, $view->total_rows);
    $view->destroy();

    // Test result count on first page of multi-page view.
    $view = $this->getPagedView(5);
    $view->setCurrentPage(0);
    $this->executeView($view);
    $this->assertEquals(18, $view->getPager()->getTotalItems());
    $this->assertEquals(18, $view->total_rows);
    $view->destroy();

    // Test result count on last page of multi-page view.
    $view = $this->getPagedView(5);
    $view->setCurrentPage(3);
    $this->executeView($view);
    $this->assertEquals(18, $view->getPager()->getTotalItems());
    $this->assertEquals(18, $view->total_rows);
    $view->destroy();
  }

  /**
   * Loads a view with the inject_results display extender and pager settings.
   *
   * @param int $items_per_page
   *   The number of items per page to set for the view's pager.
   *
   * @return \Drupal\views\ViewExecutable
   *   The initialized view.
   */
  protected function getPagedView(int $items_per_page = 0): ViewExecutable {
    $view = parent::getInjectEnabledTestView(2);
    $view->setItemsPerPage($items_per_page);

    return $view;
  }

}
