<?php

namespace Drupal\Tests\views_inject\Kernel;

use Drupal\views\ViewExecutable;

/**
 * Tests views inject functionality on views with multiple pages.
 *
 * @group views_inject
 */
class MultiPageResultsTest extends InjectResultsTestBase {

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['test_inject'];

  /**
   * {@inheritdoc}
   */
  protected int $nodeCount = 6;

  /**
   * Tests different offset values and their effects on multiple pages.
   */
  public function testOffset() {
    // Test paging with no offset.
    $this->assertPagedShorthandResultset(
      function () {
        return $this->getInjectEnabledTestView();
      },
      [
        ['article', 'page', 'article', 'page', 'article'],
        ['page', 'article', 'page', 'article', 'page'],
        ['article', 'page'],
      ]
    );

    // Test paging with offset within first page.
    $this->assertPagedShorthandResultset(
      function () {
        return $this->getInjectEnabledTestView(2);
      },
      [
        ['page', 'page', 'article', 'page', 'article'],
        ['page', 'article', 'page', 'article', 'page'],
        ['article', 'article'],
      ]
    );

    // Test paging with offset on subsequent page.
    $this->assertPagedShorthandResultset(
      function () {
        return $this->getInjectEnabledTestView(5);
      },
      [
        ['page', 'page', 'page', 'page', 'page'],
        ['article', 'page', 'article', 'article', 'article'],
        ['article', 'article'],
      ]
    );

    // Test paging with offset out of results scope.
    $this->assertPagedShorthandResultset(
      function () {
        return $this->getInjectEnabledTestView(100);
      },
      [
        ['page', 'page', 'page', 'page', 'page'],
        ['page', 'article', 'article', 'article', 'article'],
        ['article', 'article'],
      ]
    );
  }

  /**
   * Tests different chunk size values and their effects on multiple pages.
   */
  public function testChunkSize() {
    // Test paging with increased chunk size.
    $this->assertPagedShorthandResultset(
      function () {
        return $this->getInjectEnabledTestView(0, 2);
      },
      [
        ['article', 'article', 'page', 'article', 'article'],
        ['page', 'article', 'article', 'page', 'page'],
        ['page', 'page'],
      ]
    );

    // Test paging with increased chunk size causing partial end chunk.
    $this->assertPagedShorthandResultset(
      function () {
        return $this->getInjectEnabledTestView(0, 4);
      },
      [
        ['article', 'article', 'article', 'article', 'page'],
        ['article', 'article', 'page', 'page', 'page'],
        ['page', 'page'],
      ]
    );

    // Test paging with chunk size greater than page size.
    $this->assertPagedShorthandResultset(
      function () {
        return $this->getInjectEnabledTestView(0, 10);
      },
      [
        ['article', 'article', 'article', 'article', 'article'],
        ['article', 'page', 'page', 'page', 'page'],
        ['page', 'page'],
      ]
    );
  }

  /**
   * Tests different chunk distance values and their effects on multiple pages.
   */
  public function testChunkDistance() {
    // Test paging with increased chunk distance so page break is within chunk.
    $this->assertPagedShorthandResultset(
      function () {
        return $this->getInjectEnabledTestView(0, 1, 2);
      },
      [
        ['article', 'page', 'page', 'article', 'page'],
        ['page', 'article', 'page', 'page', 'article'],
        ['article', 'article'],
      ]
    );

    // Test paging with increased chunk distance causing partial end chunk.
    $this->assertPagedShorthandResultset(
      function () {
        return $this->getInjectEnabledTestView(0, 1, 4);
      },
      [
        ['article', 'page', 'page', 'page', 'page'],
        ['article', 'page', 'page', 'article', 'article'],
        ['article', 'article'],
      ]
    );

    // Test paging with increased chunk distance greater than page size.
    $this->assertPagedShorthandResultset(
      function () {
        return $this->getInjectEnabledTestView(0, 1, 10);
      },
      [
        ['article', 'page', 'page', 'page', 'page'],
        ['page', 'page', 'article', 'article', 'article'],
        ['article', 'article'],
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getInjectEnabledTestView(int $offset = 0, int $chunk_size = 1, int $chunk_distance = 1): ViewExecutable {
    $view = parent::getInjectEnabledTestView($offset, $chunk_size, $chunk_distance);
    $view->setItemsPerPage(5);

    return $view;
  }

}
