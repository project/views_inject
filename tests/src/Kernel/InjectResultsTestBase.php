<?php

namespace Drupal\Tests\views_inject\Kernel;

use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\views\Kernel\ViewsKernelTestBase;
use Drupal\views\Tests\ViewTestData;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Base class for tests involving views inject functionality.
 */
abstract class InjectResultsTestBase extends ViewsKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'views_inject_test',
    'node',
    'views_inject',
  ];

  /**
   * Available node types.
   *
   * @var string[]
   */
  protected array $nodeTypes = ['article', 'page'];

  /**
   * Number of nodes to generate per type.
   *
   * @var int
   */
  protected int $nodeCount = 2;

  /**
   * Nodes for testing.
   *
   * @var \Drupal\node\NodeInterface[][]
   */
  protected array $nodes;

  /**
   * Mapping of columns from the view result set and the expected result set.
   *
   * @var array
   */
  protected array $columnMap;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp($import_test_views = TRUE): void {
    parent::setUp($import_test_views);

    // Enable our display extender.
    $this->config('views.settings')->set('display_extenders', ['inject_results'])->save();

    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');

    // Create node types.
    foreach ($this->nodeTypes as $type) {
      $node_type = NodeType::create([
        'type' => $type,
        'name' => $type,
      ]);
      $node_type->save();
    }

    // Set column map.
    $this->columnMap = [
      'nid' => 'nid',
    ];

    // Load test views.
    ViewTestData::createTestViews(get_class($this), ['views_inject_test']);

    // Generate a clean set of nodes.
    $this->nodes = [];
    foreach ($this->nodeTypes as $type) {
      for ($count = 0; $count < $this->nodeCount; $count++) {
        $node = Node::create([
          'title' => $type . '_' . $count,
          'type' => $type,
        ]);
        $node->save();
        $this->nodes[$type][] = $node;
      }
    }
  }

  /**
   * Loads a view with the inject_results display extender enabled.
   *
   * @param int $offset
   *   The offset to use in the display extender.
   * @param int $chunk_size
   *   The chunk size to use in the display extender.
   * @param int $chunk_distance
   *   The chunk distance to use in the display extender.
   *
   * @return \Drupal\views\ViewExecutable
   *   The initialized view.
   */
  protected function getInjectEnabledTestView(int $offset = 0, int $chunk_size = 1, int $chunk_distance = 1): ViewExecutable {
    $view = Views::getView('test_inject');
    $view->initHandlers();
    $view->setDisplay('default');
    $extenders = $view->getDisplay()->getExtenders();
    $extenders['inject_results']->options['source_display'] = 'test_inject:article_embed';
    $extenders['inject_results']->options['offset'] = $offset;
    $extenders['inject_results']->options['chunk_size'] = $chunk_size;
    $extenders['inject_results']->options['chunk_distance'] = $chunk_distance;

    return $view;
  }

  /**
   * Tests if the view results are as expected.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view to test.
   * @param string[] $expected_shorthand
   *   The expected resultset, as a list of node type ids.
   * @param array|null $node_type_counter
   *   (internal) An array keyed by node type with counts for each type.
   *   Defaults to 0 for every available node type.
   */
  protected function assertShorthandResultset(ViewExecutable $view, array $expected_shorthand, array &$node_type_counter = NULL) {
    if (!isset($node_type_counter)) {
      $node_type_counter = array_fill_keys($this->nodeTypes, 0);
    }

    // Generate expected result set from shorthand.
    $expected_result = [];
    foreach ($expected_shorthand as $node_type) {
      $counter = $node_type_counter[$node_type]++;
      $expected_result[] = ['nid' => $this->nodes[$node_type][$counter]->id()];
    }

    $this->executeView($view);
    $this->assertIdenticalResultset($view, $expected_result, $this->columnMap);
    $view->destroy();
  }

  /**
   * Tests if the view results are as expected over multiple pages.
   *
   * @param callable $generate_view_callback
   *   The view to test.
   * @param string[][] $expected_shorthand
   *   The expected resultset, as a list of node type ids for each page.
   */
  protected function assertPagedShorthandResultset(callable $generate_view_callback, array $expected_shorthand) {
    $node_type_counter = array_fill_keys($this->nodeTypes, 0);
    foreach ($expected_shorthand as $page => $expected_page_content) {
      $view = $generate_view_callback();
      $view->setCurrentPage($page);
      $this->assertShorthandResultset($view, $expected_page_content, $node_type_counter);
    }
  }

}
