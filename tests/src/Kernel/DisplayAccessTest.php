<?php

namespace Drupal\Tests\views_inject\Kernel;

use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\views\ViewExecutable;

/**
 * Tests access permissions when using views inject functionality.
 *
 * @group views_inject
 */
class DisplayAccessTest extends InjectResultsTestBase {

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['test_inject'];

  /**
   * Stores a user entity with access to a restricted display.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $userWithAccess;

  /**
   * Stores a user entity without access to a restricted display.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $userWithoutAccess;

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE): void {
    parent::setUp($import_test_views);

    $role_with_access = Role::create([
      'id' => 'with_access',
      'label' => $this->randomMachineName(),
      'permissions' => ['bypass node access'],
    ]);
    $role_with_access->save();
    $role_without_access = Role::create([
      'id' => 'without_access',
      'label' => $this->randomMachineName(),
      'permissions' => [],
    ]);
    $role_without_access->save();

    $this->userWithAccess = User::create([
      'name' => $this->randomMachineName(),
      'roles' => [$role_with_access->id()],
    ]);
    $this->userWithAccess->save();
    $this->userWithoutAccess = User::create([
      'name' => $this->randomMachineName(),
      'roles' => [$role_without_access->id()],
    ]);
    $this->userWithoutAccess->save();
  }

  /**
   * Tests correct behavior in regards to injected display's access settings.
   */
  public function testInjectedDisplayAccess() {
    /** @var \Drupal\Core\Session\AccountSwitcherInterface $account_switcher */
    $account_switcher = \Drupal::service('account_switcher');

    // Test with a display we have no access to.
    $account_switcher->switchTo($this->userWithoutAccess);
    $view = $this->getInjectEnabledTestView();
    $this->assertShorthandResultset(
      $view,
      ['page', 'page']
    );

    // Test with a user that has access to the restricted display.
    $account_switcher->switchTo($this->userWithAccess);
    $view = $this->getInjectEnabledTestView();
    $this->assertShorthandResultset(
      $view,
      ['article', 'page', 'article', 'page']
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getInjectEnabledTestView(int $offset = 0, int $chunk_size = 1, int $chunk_distance = 1): ViewExecutable {
    $view = parent::getInjectEnabledTestView($offset, $chunk_size, $chunk_distance);
    $extenders = $view->getDisplay()->getExtenders();
    $extenders['inject_results']->options['source_display'] = 'test_inject:no_access_embed';

    return $view;
  }

}
