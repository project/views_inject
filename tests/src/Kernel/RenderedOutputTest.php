<?php

namespace Drupal\Tests\views_inject\Kernel;

/**
 * Tests views inject functionality related to rendered views output.
 *
 * @group views_inject
 */
class RenderedOutputTest extends InjectResultsTestBase {

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['test_inject'];

  /**
   * Tests output of fieldable views.
   *
   * @throws \Exception
   */
  public function testFieldableViews() {
    $view = $this->getInjectEnabledTestView();
    $build = $view->preview($view->current_display);
    $output = $this->render($build);

    // Find nid fields in rendered output.
    $positions = [];
    foreach ($this->nodeTypes as $node_type) {
      foreach ($this->nodes[$node_type] as $index => $node) {
        $position = strpos($output, '<span class="field-content">' . $node->id() . '</span>');
        $positions[$node_type][$index] = $position;

        // Make sure each node is found.
        $this->assertTrue($position !== FALSE);
      }
    }

    // Assert that node ids are rendered in the correct order.
    $this->assertTrue($positions['article'][0] < $positions['page'][0]);
    $this->assertTrue($positions['page'][0] < $positions['article'][1]);
    $this->assertTrue($positions['article'][1] < $positions['page'][1]);
  }

}
