Views Inject Results
====================


INTRODUCTION
------------

This module allows showing results from one views display within another with
configurable spacing. This allows you to build views where, for example, every
third row is a blog post while all others are articles, or where every _n_-th
row shows sponsored content.


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.


CONFIGURATION
-------------

The module enables its display extender functionality automatically when
installed, but you may disable it in the advanced settings tab of views,
usually found at `admin/structure/views/settings/advanced`.

Edit the view display of your choice that should have other results injected.
The center area should have a section "Inject Results" where you can alter the
module's settings for this view. These settings only apply to the currently
edited display of the view.

The following options are provided:
 * Source display: Choose the views display which contains the results to be
   injected.

 * Pass arguments to injected view: If checked, all arguments provided to the
   main view when displayed are also passed to the injected view.

 * Offset to first injected result: This is the number of rows that will be
   skipped on the first view page before the first injected result is shown.
   Choose 0 to have the first result be from the injected view.

 * Number of results to inject at a time: This is the number of rows from the
   injected view that will be shown in a batch of injected results. Choose 1
   to only show single rows with the configured spacing.

 * Number of normal view results to show between batches: This is the number of
   rows from the base view that will be shown between each batch of injected
   results.


MAINTAINERS
-----------

Current Maintainers:
 * Jörg R. (Metalbote) - https://www.drupal.org/u/metalbote
 * David I. (Mirroar) - https://www.drupal.org/u/mirroar

This project has been sponsored by:
 * werk21
   werk21 is based in Berlin, Germany and specializes in the development of
   custom web applications using Drupal. Our 20 staff members care for over 300
   clients in the range of politics and NGO.
